#pragma once

#include <cerval.h>
#include <glm/glm.hpp>

struct Position {
  float x{0.0};
  float y{0.0};
  float z{0.0};

  glm::vec3 ToGlm() const { return glm::vec3{x, y, z}; }
};

struct Size {
  float x{1.0};
  float y{1.0};

  glm::vec2 ToGlm() const { return glm::vec2{x, y}; }
};

struct Color {
  glm::vec3 color;
};

void AddSpriteBundle(entt::registry& registry, entt::entity& entity);

class QuadLayer : public Cerval::Layer {
 public:
  QuadLayer();
  virtual ~QuadLayer() override = default;
  virtual void OnEvent(Cerval::Event &e) override;
  virtual void Update() override;
  virtual void Render() override;

 private:
  int rows_{99};
  int columns_{99};
};

class TriangleLayer : public Cerval::Layer {
 public:
  TriangleLayer();
  virtual ~TriangleLayer() override = default;
  virtual void OnEvent(Cerval::Event &e) override {}
  virtual void Update() override {}
  virtual void Render() override;

 private:
  std::array<glm::vec3, 3> pos_{glm::vec3{-0.5f, -0.5f, 0.5f},
                                glm::vec3{ 0.5f, -0.5f, 0.5f},
                                glm::vec3{ 0.0f,  0.5f, 0.5f}};
  std::array<glm::vec3, 3> colors_{glm::vec3{0.2f, 0.2f, 0.8f},
                                   glm::vec3{0.8f, 0.2f, 0.2f},
                                   glm::vec3{0.2f, 0.8f, 0.2f}};
};

class ImGuiLayerIPS : public Cerval::Layer {
 public:
  ImGuiLayerIPS() : Cerval::Layer("ImGui test"), display_{true} {}
  virtual ~ImGuiLayerIPS() = default;
  virtual void OnEvent(Cerval::Event &e) override {}
  virtual void Update() override {}
  virtual void Render() override;

  bool display_;
};

class ImGuiLayerDemo : public Cerval::Layer {
 public:
  ImGuiLayerDemo() : Cerval::Layer("ImGui demo") {}
  virtual ~ImGuiLayerDemo() = default;
  virtual void OnEvent(Cerval::Event &e) override {}
  virtual void Update() override {}
  virtual void Render() override;

  bool display_{false};
};
