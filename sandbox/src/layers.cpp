#include "layers.h"

void AddSpriteBundle(entt::registry& registry,
                     entt::entity& entity,
                     const Position& position,
                     const Size& size,
                     const Color& color) {
  registry.emplace_or_replace<Position>(entity, position);
  registry.emplace_or_replace<Size>(entity, size);
  registry.emplace_or_replace<Color>(entity, color);
}

// Quad layer
QuadLayer::QuadLayer() : Cerval::Layer("Quad layer") {
  auto main_quad = registry_.create();
  AddSpriteBundle(registry_,
                  main_quad,
                  {-0.975f, -0.975f, 0.0f},
                  {0.01f, 0.01f},
                  {glm::vec3{0.8f, 0.2f, 0.2f}});
}

void QuadLayer::OnEvent(Cerval::Event &e) {
  if (e.type() == Cerval::KeyPressed) {
    switch (static_cast<Cerval::KeyPressedEvent&>(e).keycode()) {
      case KEY_J:
        rows_ -= 1;
        break;
      case KEY_L:
        rows_ += 1;
        break;
      case KEY_K:
        columns_ -= 1;
        break;
      case KEY_I:
        columns_ += 1;
        break;
      // case KEY_U:
      //   position_.z += 1.0f;
      //   break;
      // case KEY_O:
      //   position_.z -= 1.0f;
      //   break;
    }
    // position_.z = std::clamp(position_.z, -0.99f, 0.99f);
    rows_ = std::clamp(rows_, 1, 99);
    columns_ = std::clamp(columns_, 1, 99);
  }
}

void QuadLayer::Update() {
  registry_.view<Position>().each([](auto entity, auto& position){
    if (Cerval::Input::IsKeyPressed(KEY_S))
      position.x -= 0.01f;
    if (Cerval::Input::IsKeyPressed(KEY_F))
      position.x += 0.01f;
    if (Cerval::Input::IsKeyPressed(KEY_D))
      position.y -= 0.01f;
    if (Cerval::Input::IsKeyPressed(KEY_E))
      position.y += 0.01f;
  });
}

void QuadLayer::Render() {
  registry_.view<Position, Size, Color>().each([=](auto entity,
                                                   const auto& position,
                                                   const auto& size,
                                                   const auto& color){
    glm::vec3 pos{};
    for (int i{0}; i < rows_; i++) {
      for (int j{0}; j < columns_; j++) {
        pos = position.ToGlm() + glm::vec3{2.0f * i / 100, 2.0f * j / 100, 0.0f};
        Cerval::Renderer::instance().DrawRect(pos, size.ToGlm(), color.color);
      }
    }
  });
}

// Triangle layer
TriangleLayer::TriangleLayer() : Cerval::Layer("Triangle layer") {
}

void TriangleLayer::Render() {
  Cerval::Renderer::instance().DrawTriangle(
      std::array<glm::vec3, 3>{glm::vec3{0.0f, -0.5f, -0.5f},
                               glm::vec3{0.8f,  0.0f, -0.5f},
                               glm::vec3{0.0f,  0.5f, -0.5f}},
      glm::vec3{0.0f, 0.0f, 0.0f});
  Cerval::Renderer::instance().DrawTriangle(pos_, colors_);
}

// ImGui layer
void ImGuiLayerIPS::Render() {
  if (display_) {
    ImGuiIO& io{ImGui::GetIO()};
    ImGui::Begin("Test window", &display_);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / io.Framerate, io.Framerate);
    if (ImGui::Button("Close Me"))
      display_ = false;
    ImGui::End();
  }
}

void ImGuiLayerDemo::Render() {
  if (display_) {
    ImGui::ShowDemoWindow(&display_);
  }
}
