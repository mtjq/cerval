#include <cerval.h>

#include "layers.h"

std::unique_ptr<Cerval::App> Cerval::CreateApp() {
  auto app = std::make_unique<Cerval::App>("Cerval sandbox");
  ImGuiLayerIPS* ips_layer = new ImGuiLayerIPS;
  app->PushLayerFront(ips_layer);
  app->PushLayerAfter(new ImGuiLayerDemo, ips_layer);
  app->PushLayerBack(ips_layer); // should fail and produce a warning
  app->PopLayer(ips_layer);
  app->PushLayerAfter(ips_layer, "ImGui demo");
  app->PushLayerBack(new QuadLayer());
  app->PushLayerAfter(new TriangleLayer(), "Quad layer");
  return app;
}
