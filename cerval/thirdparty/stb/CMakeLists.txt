add_library(stb "${CMAKE_CURRENT_SOURCE_DIR}/stb_image.cpp")

target_include_directories(stb
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
