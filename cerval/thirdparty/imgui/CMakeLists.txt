set(source_dir "${CMAKE_CURRENT_SOURCE_DIR}")
file(GLOB_RECURSE source_files "${source_dir}/*.cpp")

add_library(imgui STATIC "${source_files}")

set(include_dir "${CMAKE_CURRENT_SOURCE_DIR}")
target_include_directories(imgui
    PUBLIC "${include_dir}")
