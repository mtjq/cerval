// Misc
#include <memory>
#include <algorithm>
#include <fstream>
#include <sstream>

// Data structures
#include <string>
#include <vector>

// Thirdparties
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>
#include <entt/entt.hpp>

// From Cerval
#include "core/log.h"
#include "spdlog/fmt/ostr.h"
