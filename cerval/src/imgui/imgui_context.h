#pragma once

namespace Cerval {
  class ImGuiContext {
   public:
    ImGuiContext() = delete;
    ~ImGuiContext() { Close(); }

    static void Init(GLFWwindow* window);
    static void PrepareRendering();
    static void Render();
    static void Close();
  };
}
