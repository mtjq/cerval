#pragma once

#include "events/event.h"

namespace Cerval {
  class Layer {
   public:
    Layer(const char* name = "Layer", bool enabled = true)
        : name_{name}, enabled_{enabled} {}
    virtual ~Layer() = default;

    std::string_view name() const { return std::string_view(name_); }
    bool enabled() const { return enabled_; }
    void Enable() { enabled_ = true; }
    void Disable() { enabled_ = false; }

    virtual void OnAttach() { CV_CORE_INFO("Attached layer {0}.", name_); }
    virtual void OnDetach() { CV_CORE_INFO("Detached layer {0}.", name_); }
    virtual void OnEvent(Event& e) = 0;
    virtual void Update() = 0;
    virtual void Render() = 0;

   protected:
    bool enabled_;
    entt::registry registry_{};

   private:
    std::string name_;
  };
}
