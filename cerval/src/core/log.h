#pragma once

#include <spdlog/spdlog.h>

namespace Cerval {
  class Log {
   public:
    static void Init();
    static std::shared_ptr<spdlog::logger> GetCoreLogger() {
      return core_logger_;}
    static std::shared_ptr<spdlog::logger> GetClientLogger() {
      return client_logger_;}

   private:
    inline static std::shared_ptr<spdlog::logger> core_logger_{};
    inline static std::shared_ptr<spdlog::logger> client_logger_{};
  };
}

#ifdef CV_LOG

#define CV_CORE_TRACE(...) Cerval::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define CV_CORE_DEBUG(...) Cerval::Log::GetCoreLogger()->debug(__VA_ARGS__)
#define CV_CORE_INFO(...) Cerval::Log::GetCoreLogger()->info(__VA_ARGS__)
#define CV_CORE_WARN(...) Cerval::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define CV_CORE_ERROR(...) Cerval::Log::GetCoreLogger()->error(__VA_ARGS__)
#define CV_CORE_CRITICAL(...) \
  Cerval::Log::GetCoreLogger()->critical(__VA_ARGS__)

#define CV_CLIENT_TRACE(...) Cerval::Log::GetClientLogger()->trace(__VA_ARGS__)
#define CV_CLIENT_DEBUG(...) Cerval::Log::GetClientLogger()->debug(__VA_ARGS__)
#define CV_CLIENT_INFO(...) Cerval::Log::GetClientLogger()->info(__VA_ARGS__)
#define CV_CLIENT_WARN(...) Cerval::Log::GetClientLogger()->warn(__VA_ARGS__)
#define CV_CLIENT_ERROR(...) Cerval::Log::GetClientLogger()->error(__VA_ARGS__)
#define CV_CLIENT_CRITICAL(...) \
  Cerval::Log::GetClientLogger()->critical(__VA_ARGS__)

#else

#define CV_CORE_TRACE(...)
#define CV_CORE_DEBUG(...)
#define CV_CORE_INFO(...)
#define CV_CORE_WARN(...)
#define CV_CORE_ERROR(...)
#define CV_CORE_CRITICAL(...)

#define CV_CLIENT_TRACE(...)
#define CV_CLIENT_DEBUG(...)
#define CV_CLIENT_INFO(...)
#define CV_CLIENT_WARN(...)
#define CV_CLIENT_ERROR(...)
#define CV_CLIENT_CRITICAL(...)

#endif

#ifdef CV_ASSERT

#define CV_CORE_ASSERT(check, msg) \
  if (!check) {CV_CORE_CRITICAL("Assert failed: {0}", msg);}
#define CV_CLIENT_ASSERT(check, msg) \
  if (!check) {CV_CLIENT_CRITICAL("Assert failed: {0}", msg);}

#else

#define CV_CORE_ASSERT(check, msg)
#define CV_CLIENT_ASSERT(check, msg)

#endif
