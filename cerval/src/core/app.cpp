#include "app.h"

#include "renderer/renderer.h"
#include "imgui/imgui_context.h"
#include "events/window_event.h"
#include "inputs/inputs.h"

namespace Cerval {
  App::App(const char* name) : name_{name} {
    CV_CORE_INFO("Creating the app.");

    WindowData data{
      name, 1920, 1200, std::bind(&App::OnEvent, this, std::placeholders::_1)};
    window_ = std::make_unique<Window>(data);

    ImGuiContext::Init(window_->window());
    Renderer::instance().Init();
    Input::Init(window_->window());
  }

  App::~App() {
    Close();
  }

  void App::Run() {
    CV_CORE_INFO("Running the app.");

    while (should_run_) {
      window_->Update();
      for (Layer* layer: layer_stack_) {
        if (layer->enabled())
          layer->Update();
      }

      Renderer::instance().PrepareRendering();
      ImGuiContext::PrepareRendering();

      for (Layer* layer: layer_stack_) {
        if (layer->enabled()) {
          layer->Render();
          Renderer::instance().Render();
        }
      }

      ImGuiContext::Render();

      Renderer::instance().SwapBuffers(window_->window());
    }
  }

  void App::OnEvent(Event& e) {
    EventDispatcher dispatcher{e};

    dispatcher.Dispatch<WindowClosedEvent>(
        static_cast<std::function<bool(WindowClosedEvent&)>>(
          [this](WindowClosedEvent& e) -> bool {
              this->SetShouldRun(false); return true;
          }
    ));

    for (auto it = layer_stack_.rbegin(); it != layer_stack_.rend(); it++) {
      if (e.handled_)
        break;
      if ((*it)->enabled())
        (*it)->OnEvent(e);
    }
  }

  void App::Close() {
    ImGuiContext::Close();
    Renderer::instance().Close();
  }
}
