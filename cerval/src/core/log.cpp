#include "log.h"

#include <spdlog/sinks/stdout_color_sinks.h>

namespace Cerval {
  void Log::Init() {
    spdlog::set_pattern("%n -> %^[%l]%$ | %T | %v");

    core_logger_ = spdlog::stdout_color_mt("CERVAL");
    core_logger_->set_level(spdlog::level::trace);

    client_logger_ = spdlog::stdout_color_mt("CLIENT");
    client_logger_->set_level(spdlog::level::trace);
  }
}
