#pragma once

#include "core/layer.h"

namespace Cerval {
  // The "front" of the stack is what is processed/rendered last, thus it is at
  // the end of the stack. Conversely, the "back" of the stack is at its
  // begining.
  class LayerStack {
   public:
    LayerStack() = default;
    ~LayerStack();

    void PushFront(Layer* layer);
    void PushBack(Layer* layer);
    void PushAfter(Layer* layer, const Layer* after);
    void PushBefore(Layer* layer, const Layer* before);
    void PushAfter(Layer* layer, std::string_view name);
    void PushBefore(Layer* layer, std::string_view name);
    void Pop(Layer* layer);

    std::vector<Layer*>::iterator begin() { return layers_.begin(); }
    std::vector<Layer*>::iterator end() { return layers_.end(); }
    std::vector<Layer*>::reverse_iterator rbegin() { return layers_.rbegin(); }
    std::vector<Layer*>::reverse_iterator rend() { return layers_.rend(); }
    std::vector<Layer*>::const_iterator begin() const {
      return layers_.begin(); }
    std::vector<Layer*>::const_iterator end() const {
      return layers_.end(); }
    std::vector<Layer*>::const_reverse_iterator rbegin() const {
      return layers_.rbegin(); }
    std::vector<Layer*>::const_reverse_iterator rend() const {
      return layers_.rend(); }

   private:
    std::vector<Layer*> layers_{};
  };
}
