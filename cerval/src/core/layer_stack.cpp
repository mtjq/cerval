#include "core/layer_stack.h"

namespace Cerval {
  LayerStack::~LayerStack() {
    for (Layer* layer: layers_) {
      layer->OnDetach();
      delete layer;
    }
  }

  void LayerStack::PushFront(Layer* layer) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    layers_.push_back(layer);
    layer->OnAttach();
  }

  void LayerStack::PushBack(Layer* layer) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    layers_.insert(layers_.begin(), layer);
    layer->OnAttach();
  }

  void LayerStack::PushAfter(Layer* layer, const Layer* after) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    it = std::find(layers_.begin(), layers_.end(), after);
    if (it == layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" cannot be found, and thus \"{1}\" has not "
          "been added.", after->name(), layer->name());
      return;
    }

    layers_.insert(it + 1, layer);
    layer->OnAttach();
  }

  void LayerStack::PushBefore(Layer* layer, const Layer* before) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    it = std::find(layers_.begin(), layers_.end(), before);
    if (it == layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" cannot be found, and thus \"{1}\" has not "
          "been added.", before->name(), layer->name());
      return;
    }

    layers_.insert(it, layer);
    layer->OnAttach();
  }

  void LayerStack::PushAfter(Layer* layer, std::string_view name) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    it = std::find_if(layers_.begin(), layers_.end(), [name](Layer* l) -> bool {
        return l->name() == name; });
    if (it == layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" cannot be found, and thus \"{1}\" has not "
          "been added.", name, layer->name());
      return;
    }

    layers_.insert(it + 1, layer);
    layer->OnAttach();
  }

  void LayerStack::PushBefore(Layer* layer, std::string_view name) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it != layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is already in the layer stack and has not "
          "been added.", layer->name());
      return;
    }

    it = std::find_if(layers_.begin(), layers_.end(), [name](Layer* l) -> bool {
        return l->name() == name; });
    if (it == layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" cannot be found, and thus \"{1}\" has not "
          "been added.", name, layer->name());
      return;
    }

    layers_.insert(it, layer);
    layer->OnAttach();
  }

  void LayerStack::Pop(Layer* layer) {
    auto it{std::find(layers_.begin(), layers_.end(), layer)};
    if (it == layers_.end()) {
      CV_CORE_WARN("Layer \"{0}\" is not in the layer stack and has not "
          "been removed.", layer->name());
      return;
    }

    layers_.erase(it);
    layer->OnDetach();
  }
}
