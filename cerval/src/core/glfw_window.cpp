#include "glfw_window.h"
#include "events/window_event.h"
#include "events/key_event.h"
#include "events/mouse_event.h"

namespace Cerval {
  Window::Window(WindowData data) : data_{data} {
    if (!window_count_) {
      // Initialising GLFW
      CV_CORE_INFO("Initialising glfw.");
      int glfw_success{glfwInit()};
      CV_CLIENT_ASSERT(glfw_success, "Failed to initialise glfw.");
      glfwSwapInterval(1); // Enable vsync

      // Create window
      glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
      glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    }

    CV_CORE_INFO("Creating window.");
    window_ = glfwCreateWindow(
        data_.width, data_.height, data_.title, NULL, NULL);
    CV_CLIENT_ASSERT(window_, "Failed to create GLFW window");
    glfwSetWindowUserPointer(window_, &data_);
    glfwMakeContextCurrent(window_);

    if (!window_count_) {
      // Glad: load all OpenGL function pointers
      CV_CORE_INFO("Initialising glad");
      bool glad_success = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
      CV_CLIENT_ASSERT(glad_success, "Failed to initialize GLAD");

      CV_CORE_INFO("OpenGL Info:");
      CV_CORE_INFO("  Vendor: {}",
          reinterpret_cast<const char *>(glGetString(GL_VENDOR)));
      CV_CORE_INFO("  Renderer: {}",
          reinterpret_cast<const char *>(glGetString(GL_RENDERER)));
      CV_CORE_INFO("  Version: {}",
          reinterpret_cast<const char *>(glGetString(GL_VERSION)));
    }

    glViewport(0, 0, data_.width, data_.height);
    // Event callbacks
    glfwSetFramebufferSizeCallback(window_, framebuffer_size_callback);

    glfwSetWindowCloseCallback(window_, [](GLFWwindow* window){
        WindowClosedEvent e;
        WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
        data.callback(e);
    });

    glfwSetWindowSizeCallback(
        window_, [](GLFWwindow* window, int width, int height){
          WindowResizedEvent e{width, height};
          WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
          data.width = width;
          data.height = height;

          data.callback(e);
    });

    glfwSetKeyCallback(
        window_, [](GLFWwindow* window,
                    int key, int scancode, int action, int mods){
          WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
          bool repeated{false};
          switch (action) {
            case GLFW_RELEASE:
              {
                KeyReleasedEvent e{key};
                data.callback(e);
              }
              break;
            case GLFW_REPEAT:
              repeated = true;
            case GLFW_PRESS:
              {
                KeyPressedEvent e{key, repeated};
                data.callback(e);
              }
              break;
          }
    });

    glfwSetCursorPosCallback(
        window_, [](GLFWwindow* window, double xpos, double ypos){
          MouseMovedEvent e{xpos, ypos};
          WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
          data.callback(e);
    });

    glfwSetScrollCallback(
        window_, [](GLFWwindow* window, double xoffset, double yoffset){
          MouseScrolledEvent e{yoffset, yoffset};
          WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
          data.callback(e);
    });

    glfwSetMouseButtonCallback(
        window_, [](GLFWwindow* window, int button, int action, int mods){
          WindowData data = *(WindowData*)glfwGetWindowUserPointer(window);
          switch (action) {
            case GLFW_PRESS:
              {
                MouseButtonPressedEvent e{button};
                data.callback(e);
              }
              break;
            case GLFW_RELEASE:
              {
                MouseButtonReleasedEvent e{button};
                data.callback(e);
              }
              break;
          }
    });

    window_count_++;
  }

  Window::~Window() {
    CV_CORE_INFO("Destroying window.");
    glfwDestroyWindow(window_);
  }


  void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
  }
}
