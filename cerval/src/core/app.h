#pragma once

#include "core/glfw_window.h"
#include "events/event.h"
#include "core/layer_stack.h"

namespace Cerval {
  class App {
   public:
    App(const char* name = "Cerval app");
    ~App();

    void SetShouldRun(bool should_run) { should_run_ = should_run; }

    void Run();
    void PushLayerFront(Layer* layer) { layer_stack_.PushFront(layer); }
    void PushLayerBack(Layer* layer) { layer_stack_.PushBack(layer); }
    void PushLayerBefore(Layer* layer, const Layer* before) {
      layer_stack_.PushBefore(layer, before); }
    void PushLayerAfter(Layer* layer, const Layer* after) {
      layer_stack_.PushAfter(layer, after); }
    void PushLayerBefore(Layer* layer, std::string_view before) {
      layer_stack_.PushBefore(layer, before); }
    void PushLayerAfter(Layer* layer, std::string_view after) {
      layer_stack_.PushAfter(layer, after); }
    void PopLayer(Layer* layer) { layer_stack_.Pop(layer); }
    void OnEvent(Event& e);
    void Close();

   private:
    const char* name_;
    std::unique_ptr<Window> window_{nullptr};
    LayerStack layer_stack_{};
    bool should_run_{true};
  };

  // Defined by the client
  std::unique_ptr<App> CreateApp();
}
