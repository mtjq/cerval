#pragma once

#include "events/event.h"

namespace Cerval {
  struct WindowData {
    const char* title;
    int width;
    int height;
    std::function<void(Event&)> callback;
  };

  class Window {
   public:
    Window(WindowData data);
    ~Window();

    GLFWwindow* window() const { return window_; }

    void Update() { glfwPollEvents(); }

   private:
    // For now, we are not really supposed to spawn more than one window, but
    // this ensures that we initialise glfw/glad only once
    inline static unsigned int window_count_{0};
    GLFWwindow* window_{nullptr};
    WindowData data_;
  };

  void framebuffer_size_callback(GLFWwindow* window, int width, int height);
}
