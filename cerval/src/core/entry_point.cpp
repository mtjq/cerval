#include "core/app.h"

int main() {
  Cerval::Log::Init();
  CV_CORE_INFO("Cerval entry point");

  auto app = Cerval::CreateApp();
  app->Run();

  CV_CORE_INFO("Cerval exit point");
  return 0;
}
