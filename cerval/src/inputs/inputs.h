#pragma once

namespace Cerval {
  class Input {
   public:
    Input() = delete;

    static void Init(GLFWwindow* window) { window_ = window; }
    static bool IsKeyPressed(int key) {
      return glfwGetKey(window_, key) == GLFW_PRESS; }

   private:
    inline static GLFWwindow* window_{};
  };
}
