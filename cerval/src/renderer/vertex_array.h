#pragma once

#include "renderer/buffer.h"

namespace Cerval {
  class VertexArray {
   public:
    VertexArray() { glGenVertexArrays(1, &id_); }
    VertexArray(std::shared_ptr<VertexBuffer> vertex_buffer,
                std::shared_ptr<IndexBuffer> index_buffer);
    ~VertexArray() { glDeleteVertexArrays(1, &id_); }

    void SetBuffers(std::shared_ptr<VertexBuffer> vertex_buffer,
                    std::shared_ptr<IndexBuffer> index_buffer);
    void Bind() const { glBindVertexArray(id_); }
    void Unbind() const { glBindVertexArray(0); }

   private:
    void DefineVertexAttributes() const;

    unsigned int id_{0};
    std::shared_ptr<VertexBuffer> vertex_buffer_{nullptr};
    std::shared_ptr<IndexBuffer> index_buffer_{nullptr};
  };
}
