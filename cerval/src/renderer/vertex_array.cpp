#include "vertex_array.h"

namespace Cerval {
  // Vertex array
  VertexArray::VertexArray(std::shared_ptr<VertexBuffer> vertex_buffer,
                           std::shared_ptr<IndexBuffer> index_buffer)
      : vertex_buffer_{vertex_buffer},
        index_buffer_{index_buffer} {
    glGenVertexArrays(1, &id_);
    DefineVertexAttributes();
  }

  void VertexArray::SetBuffers(std::shared_ptr<VertexBuffer> vertex_buffer,
      std::shared_ptr<IndexBuffer> index_buffer) {
    vertex_buffer_ = vertex_buffer;
    index_buffer_ = index_buffer;
    DefineVertexAttributes();
  }

  void VertexArray::DefineVertexAttributes() const {
    Bind();
    vertex_buffer_->Bind();
    if (index_buffer_)
      index_buffer_->Bind();
    unsigned int idx{0};
    for (const auto& att: vertex_buffer_->layout()) {
      switch (att.type) {
        case ShaderDataType::Bool:
        case ShaderDataType::Int:
        case ShaderDataType::Int2:
        case ShaderDataType::Int3:
        case ShaderDataType::Int4:
          glEnableVertexAttribArray(idx);
          glVertexAttribIPointer(
              idx, att.count, GL_INT, att.stride, (const void*)att.offset);
          break;
        case ShaderDataType::Float:
        case ShaderDataType::Float2:
        case ShaderDataType::Float3:
        case ShaderDataType::Float4:
          glEnableVertexAttribArray(idx);
          glVertexAttribPointer(
              idx, att.count, GL_FLOAT, att.normalized ? GL_TRUE : GL_FALSE,
              att.stride, (const void*)att.offset);
          break;
        case ShaderDataType::Mat3:
        case ShaderDataType::Mat4:
          {
            int count = att.count;
            for (int i = 0; i < count; i++)
            {
              glEnableVertexAttribArray(idx);
              glVertexAttribPointer(
                  idx,
                  count,
                  GL_FLOAT,
                  att.normalized ? GL_TRUE : GL_FALSE,
                  att.stride,
                  (const void*)(att.offset + sizeof(float) * count * i));
              glVertexAttribDivisor(idx, 1);
              idx++;
            }
            break;
          }
        default:
          CV_CORE_ASSERT(false, "Unknown shader data type.");
          break;
      }
      idx++;
    }
  }
}
