#include "buffer.h"

namespace Cerval {
  size_t ShaderDataTypeSize(ShaderDataType type) {
    switch (type) {
      case ShaderDataType::Int:     return sizeof(int) * 1;
      case ShaderDataType::Int2:    return sizeof(int) * 2;
      case ShaderDataType::Int3:    return sizeof(int) * 3;
      case ShaderDataType::Int4:    return sizeof(int) * 4;
      case ShaderDataType::Float:   return sizeof(float) * 1;
      case ShaderDataType::Float2:  return sizeof(float) * 2;
      case ShaderDataType::Float3:  return sizeof(float) * 3;
      case ShaderDataType::Float4:  return sizeof(float) * 4;
      case ShaderDataType::Mat3:    return sizeof(float) * 3 * 3;
      case ShaderDataType::Mat4:    return sizeof(float) * 4 * 4;
      case ShaderDataType::Bool:    return sizeof(bool);
      default:
        CV_CORE_ASSERT(false, "Unknown shader data type.");
        return 0;
    }
  }

  int ShaderDataTypeCount(ShaderDataType type) {
    switch (type) {
      case ShaderDataType::Int:     return 1;
      case ShaderDataType::Int2:    return 2;
      case ShaderDataType::Int3:    return 3;
      case ShaderDataType::Int4:    return 4;
      case ShaderDataType::Float:   return 1;
      case ShaderDataType::Float2:  return 2;
      case ShaderDataType::Float3:  return 3;
      case ShaderDataType::Float4:  return 4;
      case ShaderDataType::Mat3:    return 3; // 3 * Float3
      case ShaderDataType::Mat4:    return 4; // 4 * Float4
      case ShaderDataType::Bool:    return 1;
      default:
        CV_CORE_ASSERT(false, "Unknown shader data type.");
        return 0;
    }
  }

  void VertexLayout::ComputeAttributesData() {
    size_t offset{0};
    unsigned int location{0};
    for (auto& attribute: attributes_) {
      attribute.location = location;
      attribute.offset = offset;
      location++;
      offset += attribute.size;
    }
    for (auto& attribute: attributes_) {
      attribute.stride = offset;
    }
  }

  // Vertex buffer
  VertexBuffer::VertexBuffer(
      size_t size, const float* vertices, VertexLayout layout) {
    glGenBuffers(1, &id_);
    Bind();
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    layout_ = layout;
  }

  void VertexBuffer::SetData(
      size_t size, const float* vertices, VertexLayout layout) {
    Bind();
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_DYNAMIC_DRAW);
    layout_ = layout;
  }

  // Index buffer
  IndexBuffer::IndexBuffer(size_t size, const unsigned int* indices) : id_{} {
    glGenBuffers(1, &id_);
    Bind();
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
  }
}
