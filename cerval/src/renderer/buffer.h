#pragma once

namespace Cerval {
  enum class ShaderDataType {
    None = 0,
    Int, Int2, Int3, Int4,
    Float, Float2, Float3, Float4,
    Mat3, Mat4,
    Bool
  };

  size_t ShaderDataTypeSize(ShaderDataType type);
  int ShaderDataTypeCount(ShaderDataType type);

  // Vertex attribute
  struct VertexAttribute {
    VertexAttribute() = default;
    VertexAttribute(ShaderDataType type, bool normalized = true)
        : type{type},
          size{ShaderDataTypeSize(type)},
          count{ShaderDataTypeCount(type)},
          normalized{normalized} {}
    ~VertexAttribute() = default;

    ShaderDataType type;
    size_t size;
    int count;
    bool normalized;
    unsigned int location{0};
    size_t stride{0};
    size_t offset{0};
  };

  // Vertex layout
  class VertexLayout {
   public:
    VertexLayout() = default;
    VertexLayout(std::initializer_list<VertexAttribute> attributes)
        : attributes_{attributes} { ComputeAttributesData(); }
    ~VertexLayout() = default;

    const std::vector<VertexAttribute>& attributes() const {
      return attributes_; }
    std::vector<VertexAttribute>::iterator begin() {
      return attributes_.begin(); }
    std::vector<VertexAttribute>::iterator end() {
      return attributes_.end(); }
    std::vector<VertexAttribute>::const_iterator begin() const {
      return attributes_.begin(); }
    std::vector<VertexAttribute>::const_iterator end() const {
      return attributes_.end(); }

   private:
    void ComputeAttributesData();

    std::vector<VertexAttribute> attributes_;
  };

  // Vertex buffer
  class VertexBuffer {
   public:
    VertexBuffer() { glGenBuffers(1, &id_); }
    VertexBuffer(size_t size, const float* vertices, VertexLayout layout);
    ~VertexBuffer() { glDeleteBuffers(1, &id_); }

    VertexLayout layout() { return layout_; }

    void SetData(size_t size, const float* vertices, VertexLayout layout);
    void Bind() const { glBindBuffer(GL_ARRAY_BUFFER, id_); }
    void Unbind() const { glBindBuffer(GL_ARRAY_BUFFER, 0); }

   private:
    unsigned int id_{0};
    VertexLayout layout_{};
  };

  // Index buffer
  class IndexBuffer {
   public:
    IndexBuffer() { glGenBuffers(1, &id_); }
    IndexBuffer(size_t size, const unsigned int* indices);
    ~IndexBuffer() { glDeleteBuffers(1, &id_); }

    void Bind() const { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_); }
    void Unbind() const { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); }

   private:
    unsigned int id_{0};
  };
}
