#include "shader.h"


namespace Cerval {
  Shader::Shader(const char* vertex_path, const char* fragment_path) {
    SetShaders(vertex_path, fragment_path);
  }

  void Shader::SetShaders(const char* vertex_path, const char* fragment_path) {
    id_ = glCreateProgram();

    const std::string vertex_code{ReadFile(vertex_path)};
    const std::string fragment_code{ReadFile(fragment_path)};

    unsigned int vertex_id =
      CompileShader(GL_VERTEX_SHADER, &vertex_code[0]);
    unsigned int fragment_id =
      CompileShader(GL_FRAGMENT_SHADER, &fragment_code[0]);

    LinkProgram(vertex_id, fragment_id);
  }

  unsigned int Shader::CompileShader(unsigned int type, const char* code) {
    int success;
    char infoLog[512];

    unsigned int id = glCreateShader(type);
    glShaderSource(id, 1, &code, NULL);
    glCompileShader(id);
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if(!success) {
      glGetShaderInfoLog(id, 512, NULL, infoLog);
      CV_CORE_ERROR("Shader compilation failed with error: {0}", infoLog);
    }

    return id;
  }

  void Shader::LinkProgram(
      unsigned int vertex_id, unsigned int fragment_id) {
    int success;
    char infoLog[512];

    glAttachShader(id_, vertex_id);
    glAttachShader(id_, fragment_id);
    glLinkProgram(id_);
    glGetProgramiv(id_, GL_LINK_STATUS, &success);
    if(!success) {
      glGetProgramInfoLog(id_, 512, NULL, infoLog);
      CV_CORE_ERROR("Shader program linking failed with error: {0}", infoLog);
    }

    glDeleteShader(vertex_id);
    glDeleteShader(fragment_id);
  }

  std::string Shader::ReadFile(const std::string& filepath)
  {
    std::string result;
    std::ifstream in(filepath, std::ios::in | std::ios::binary);
    if (!in) {
      CV_CORE_ERROR("Could not open file \"{0}\".", filepath);
      return "";
    }

    in.seekg(0, std::ios::end);
    size_t size = in.tellg();
    if (size < 0) {
      CV_CORE_ERROR("Could not read from file \"{0}\".", filepath);
      return "";
    }

    result.resize(size);
    in.seekg(0, std::ios::beg);
    in.read(&result[0], size);

    return result;
  }
}
