#pragma once

#include <glm/gtc/type_ptr.hpp>

namespace Cerval {
  class Shader {
   public:
    Shader() = default;
    Shader(const char* vertex_path, const char* fragment_path);
    ~Shader() { glDeleteProgram(id_); }

    void SetShaders(const char* vertex_path, const char* fragment_path);
    void Bind() { glUseProgram(id_); }
    void UnBind() { glUseProgram(0); }
    void SetBool(const std::string &name, bool value) const {
      glUniform1i(glGetUniformLocation(id_, name.c_str()), (int)value);
    }
    void SetInt(const std::string &name, int value) const {
      glUniform1i(glGetUniformLocation(id_, name.c_str()), value);
    }
    void SetFloat(const std::string &name, float value) const {
      glUniform1f(glGetUniformLocation(id_, name.c_str()), value);
    }
    void Set4Float(
        const std::string &name, float v1, float v2, float v3, float v4) const {
      glUniform4f(glGetUniformLocation(id_, name.c_str()), v1, v2, v3, v4);
    }
    void SetMatrix4(const std::string &name, const glm::mat4 &mat) const {
      glUniformMatrix4fv(glGetUniformLocation(id_, name.c_str()), 1, GL_FALSE,
                         glm::value_ptr(mat));
    }

   private:
    std::string ReadFile(const std::string& filepath);
    unsigned int CompileShader(unsigned int type, const char* code);
    void LinkProgram(unsigned int vertex_id, unsigned int fragment_id);

    unsigned int id_{};
  };
}
