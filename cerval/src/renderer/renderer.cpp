#include "renderer.h"

#include <iostream>

namespace Cerval {
  void Renderer::Init() {
    CV_CORE_INFO("Initialising renderer.");

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    // Triangles
    // Vertices
    rdata_.triangle_vertices =
      new TriangleVertex[rdata_.triangle_vertices_count_max];
    rdata_.triangle_vertices_base = rdata_.triangle_vertices;

    VertexLayout triangle_layout{VertexAttribute{ShaderDataType::Float3},
                                 VertexAttribute{ShaderDataType::Float3}};

    // Vertex buffer
    rdata_.triangle_vertex_buffer->SetData(
        rdata_.triangle_vertices_count_max * sizeof(TriangleVertex),
        nullptr,
        triangle_layout);

    // Indices
    unsigned int* triangle_indices =
      new unsigned int[rdata_.triangle_indices_count_max];
    for (int i{0}; i < rdata_.triangle_indices_count_max; i++)
      triangle_indices[i] = i;

    // Vertex array
    rdata_.triangle_vertex_array->SetBuffers(
        rdata_.triangle_vertex_buffer, nullptr);
    rdata_.triangle_vertex_array->Unbind();


    // Quads
    // Vertices
    rdata_.quad_vertices = new QuadVertex[rdata_.quad_vertices_count_max];
    rdata_.quad_vertices_base = rdata_.quad_vertices;

    VertexLayout quad_layout{VertexAttribute{ShaderDataType::Float3},
                             VertexAttribute{ShaderDataType::Float3}};

    // Vertex buffer
    rdata_.quad_vertex_buffer->SetData(
        rdata_.quad_vertices_count_max * sizeof(QuadVertex),
        nullptr,
        quad_layout);

    // Indices
    unsigned int* quad_indices =
      new unsigned int[rdata_.quad_indices_count_max];
    for (int i{0}; i < rdata_.quad_max_count; i++) {
      quad_indices[6 * i]     = 4 * i;
      quad_indices[6 * i + 1] = 4 * i + 1;
      quad_indices[6 * i + 2] = 4 * i + 2;

      quad_indices[6 * i + 3] = 4 * i + 2;
      quad_indices[6 * i + 4] = 4 * i + 3;
      quad_indices[6 * i + 5] = 4 * i;
    }

    // Index buffer
    std::shared_ptr<IndexBuffer> quad_index_buffer =
      std::make_shared<IndexBuffer>(
          rdata_.quad_indices_count_max * sizeof(float), quad_indices);

    // Vertex array
    rdata_.quad_vertex_array->SetBuffers(rdata_.quad_vertex_buffer,
                                          quad_index_buffer);
    rdata_.quad_vertex_array->Unbind();

    // Rect vertex positions
    rdata_.rect_vertex_positions[0] = glm::vec4{-0.5f, -0.5f, 0.0f, 1.0f};
    rdata_.rect_vertex_positions[1] = glm::vec4{ 0.5f, -0.5f, 0.0f, 1.0f};
    rdata_.rect_vertex_positions[2] = glm::vec4{ 0.5f,  0.5f, 0.0f, 1.0f};
    rdata_.rect_vertex_positions[3] = glm::vec4{-0.5f,  0.5f, 0.0f, 1.0f};
  }

  void Renderer::PrepareRendering() {
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }

  void Renderer::Render() {
    // Triangles
    shader_.Bind();
    rdata_.triangle_vertex_array->Bind();
    rdata_.triangle_vertex_buffer->Bind();
    glBufferSubData(GL_ARRAY_BUFFER,
                    0,
                    rdata_.triangle_count * 3 * sizeof(TriangleVertex),
                    rdata_.triangle_vertices_base);
    glDrawArrays(GL_TRIANGLES, 0, rdata_.triangle_count * 3);

    rdata_.triangle_vertices = rdata_.triangle_vertices_base;
    rdata_.triangle_count = 0;

    // Quads
    shader_.Bind();
    rdata_.quad_vertex_array->Bind();
    rdata_.quad_vertex_buffer->Bind();
    glBufferSubData(GL_ARRAY_BUFFER,
                    0,
                    rdata_.quad_count * 4 * sizeof(QuadVertex),
                    rdata_.quad_vertices_base);
    glDrawElements(GL_TRIANGLES, rdata_.quad_count * 6, GL_UNSIGNED_INT, 0);

    rdata_.quad_vertices = rdata_.quad_vertices_base;
    rdata_.quad_count = 0;
  }

  void Renderer::Close() {
    CV_CORE_INFO("Terminating glfw.");
    glfwTerminate();
  }

  // Draw triangles
  void Renderer::DrawTriangle(const std::array<glm::vec3, 3>& pos,
                              const std::array<glm::vec3, 3>& colors) {
    for (int i{0}; i < 3; i++) {
      rdata_.triangle_vertices->position = pos[i];
      rdata_.triangle_vertices->color = colors[i];
      rdata_.triangle_vertices++;
    }
    rdata_.triangle_count++;
  }

  void Renderer::DrawTriangle(const std::array<glm::vec3, 3>& pos,
                              const glm::vec3& color) {
    DrawTriangle(pos, std::array<glm::vec3, 3>{color, color, color});
  }

  void Renderer::DrawTriangle(const std::array<glm::vec2, 3>& pos,
                              const std::array<glm::vec3, 3>& colors) {
    for (int i{0}; i < 3; i++) {
      rdata_.triangle_vertices->position = glm::vec3(pos[i], 0.0f);
      rdata_.triangle_vertices->color = colors[i];
      rdata_.triangle_vertices++;
    }
    rdata_.triangle_count++;
  }

  void Renderer::DrawTriangle(const std::array<glm::vec2, 3>& pos,
                              const glm::vec3& color) {
    DrawTriangle(pos, std::array<glm::vec3, 3>{color, color, color});
  }

  // Draw rectangles
  void Renderer::DrawRect(const glm::vec3& position,
                          const glm::vec2& size,
                          const std::array<glm::vec3, 4>& colors) {
    glm::mat4 trans =
      glm::scale(glm::translate(glm::mat4{1.0f}, position),
                 glm::vec3{size, 1.0f});

    for (size_t i{0}; i < 4; i++) {
      rdata_.quad_vertices->position =
        trans * rdata_.rect_vertex_positions[i];
      rdata_.quad_vertices->color = colors[i];

      rdata_.quad_vertices++;
    }

    rdata_.quad_count++;
  }

  void Renderer::DrawRect(const glm::vec3& position,
                          const glm::vec2& size,
                          const glm::vec3& color) {
    DrawRect(position, size,
             std::array<glm::vec3, 4>{color, color, color, color});
  }

  void Renderer::DrawRect(const glm::vec2& position,
                          const glm::vec2& size,
                          const std::array<glm::vec3, 4>& colors) {
    glm::mat4 trans =
      glm::scale(glm::translate(glm::mat4{1.0f}, glm::vec3{position, 0.0f}),
                 glm::vec3{size, 1.0f});

    for (size_t i{0}; i < 4; i++) {
      rdata_.quad_vertices->position =
        trans * rdata_.rect_vertex_positions[i];
      rdata_.quad_vertices->color = colors[i];

      rdata_.quad_vertices++;
    }

    rdata_.quad_count++;
  }

  void Renderer::DrawRect(const glm::vec2& position,
                          const glm::vec2& size,
                          const glm::vec3& color) {
    DrawRect(position, size,
             std::array<glm::vec3, 4>{color, color, color, color});
  }

  // Draw quads
  void Renderer::DrawQuad(const std::array<glm::vec3, 4>& positions,
                          const std::array<glm::vec3, 4>& colors) {
    for (size_t i{0}; i < 4; i++) {
      rdata_.quad_vertices->position = positions[i];
      rdata_.quad_vertices->color = colors[i];

      rdata_.quad_vertices++;
    }

    rdata_.quad_count++;
  }

  void Renderer::DrawQuad(const std::array<glm::vec3, 4>& positions,
                          const glm::vec3& color) {
    DrawQuad(positions, std::array<glm::vec3, 4>{color, color, color, color});
  }

  void Renderer::DrawQuad(const std::array<glm::vec2, 4>& positions,
                          const std::array<glm::vec3, 4>& colors) {
    for (size_t i{0}; i < 4; i++) {
      rdata_.quad_vertices->position = glm::vec3{positions[i], 0.0f};
      rdata_.quad_vertices->color = colors[i];

      rdata_.quad_vertices++;
    }

    rdata_.quad_count++;
  }

  void Renderer::DrawQuad(const std::array<glm::vec2, 4>& positions,
                          const glm::vec3& color) {
    DrawQuad(positions, std::array<glm::vec3, 4>{color, color, color, color});
  }
}
