#pragma once

#include "renderer/buffer.h"
#include "renderer/vertex_array.h"
#include "renderer/shader.h"

namespace Cerval {
  struct TriangleVertex {
    glm::vec3 position;
    glm::vec3 color;
  };

  struct QuadVertex {
    glm::vec3 position;
    glm::vec3 color;
  };

  struct RenderData {
    // Triangles
    inline static size_t triangle_max_count{10'000};
    inline static size_t triangle_vertices_count_max{3 * triangle_max_count};
    inline static size_t triangle_indices_count_max{3 * triangle_max_count};

    std::shared_ptr<VertexArray> triangle_vertex_array{
      std::make_shared<VertexArray>()};
    std::shared_ptr<VertexBuffer> triangle_vertex_buffer{
      std::make_shared<VertexBuffer>()};

    TriangleVertex* triangle_vertices{nullptr};
    TriangleVertex* triangle_vertices_base{nullptr};
    size_t triangle_count{0};

    // Quads
    inline static size_t quad_max_count{10'000};
    inline static size_t quad_vertices_count_max{4 * quad_max_count};
    inline static size_t quad_indices_count_max{6 * quad_max_count};

    std::shared_ptr<VertexArray> quad_vertex_array{
      std::make_shared<VertexArray>()};
    std::shared_ptr<VertexBuffer> quad_vertex_buffer{
      std::make_shared<VertexBuffer>()};

    QuadVertex* quad_vertices{nullptr};
    QuadVertex* quad_vertices_base{nullptr};
    size_t quad_count{0};

    glm::vec4 rect_vertex_positions[4];
  };

  class Renderer {
   public:
    ~Renderer() = default;

    static Renderer& instance() { static Renderer instance{}; return instance; }
    void Init();
    void PrepareRendering();
    void Render();
    void SwapBuffers(GLFWwindow* window) { glfwSwapBuffers(window); }
    void Close();

    void DrawTriangle(const std::array<glm::vec3, 3>& pos,
                      const std::array<glm::vec3, 3>& colors);
    void DrawTriangle(const std::array<glm::vec3, 3>& pos,
                      const glm::vec3& color);
    void DrawTriangle(const std::array<glm::vec2, 3>& pos,
                      const std::array<glm::vec3, 3>& colors);
    void DrawTriangle(const std::array<glm::vec2, 3>& pos,
                      const glm::vec3& color);
    void DrawRect(const glm::vec3& position,
                  const glm::vec2& size,
                  const std::array<glm::vec3, 4>& colors);
    void DrawRect(const glm::vec3& position,
                  const glm::vec2& size,
                  const glm::vec3& color);
    void DrawRect(const glm::vec2& position,
                  const glm::vec2& size,
                  const std::array<glm::vec3, 4>& colors);
    void DrawRect(const glm::vec2& position,
                  const glm::vec2& size,
                  const glm::vec3& color);
    void DrawQuad(const std::array<glm::vec3, 4>& positions,
                  const std::array<glm::vec3, 4>& colors);
    void DrawQuad(const std::array<glm::vec3, 4>& positions,
                  const glm::vec3& color);
    void DrawQuad(const std::array<glm::vec2, 4>& positions,
                  const std::array<glm::vec3, 4>& colors);
    void DrawQuad(const std::array<glm::vec2, 4>& positions,
                  const glm::vec3& color);

   private:
    Renderer() = default;

    RenderData rdata_{};
    Shader shader_{"cerval/assets/shaders/quad_vertex.glsl",
                   "cerval/assets/shaders/quad_fragment.glsl"};
  };
}
