#pragma once

#include "events/event.h"

namespace Cerval {
  class KeyEvent : public Event {
   public:
    int keycode() { return keycode_; }

    static EventCategory static_category() { return EventCategory::Keyboard; }
    virtual EventCategory category() const override {
      return EventCategory::Keyboard;
    }

   protected:
    KeyEvent(int keycode)
        : keycode_{keycode} {}
    int keycode_;
  };

  class KeyPressedEvent : public KeyEvent {
   public:
    KeyPressedEvent(int keycode, bool repeated)
      : KeyEvent(keycode), repeated_{repeated} {}

    static EventType static_type() { return KeyPressed; }
    virtual EventType type() const override { return KeyPressed; }
    virtual std::string ToString() const override;
   private:
    bool repeated_;
  };

  class KeyReleasedEvent : public KeyEvent {
   public:
    KeyReleasedEvent(int keycode) : KeyEvent(keycode) {}

    static EventType static_type() { return KeyReleased; }
    virtual EventType type() const override { return KeyReleased; }
    virtual std::string ToString() const override;
  };
}
