#pragma once

namespace Cerval {
  enum EventType {
    None = 0,
    WindowResize, WindowClose,
    KeyPressed, KeyReleased,
    MouseMoved, MouseScrolled, MouseButtonPressed, MouseButtonReleased
  };

  enum class EventCategory {
    None = 0,
    Window, Keyboard, Mouse, MouseButton
  };

  // Event
  class Event {
   public:
    virtual ~Event() = default;

    virtual EventCategory category() const = 0;
    virtual EventType type() const = 0;
    virtual std::string ToString() const { return std::string("Empty event"); }

    bool handled_{false};
  };

  // Dispatcher
  class EventDispatcher {
   public:
    EventDispatcher(Event& e) : event_{e} {}
    ~EventDispatcher() = default;

    template<typename E, typename FE>
    void Dispatch(const std::function<bool(FE&)>& func);

   private:
    Event& event_;
  };

  inline std::ostream& operator<<(std::ostream& os, const Event& e) {
    return os << e.ToString();
  }
}

namespace Cerval {
  template<typename E, typename FE>
  void EventDispatcher::Dispatch(const std::function<bool(FE&)>& func) {
    if (event_.type() == E::static_type()) {
      event_.handled_ |= func(static_cast<FE&>(event_));
    }
  }
}
