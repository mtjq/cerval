#pragma once

#include "events/event.h"

namespace Cerval {
  class WindowEvent : public Event {
   public:
    static EventCategory static_category() { return EventCategory::Window; }
    virtual EventCategory category() const override {
      return EventCategory::Window;
    };
  };

  class WindowClosedEvent : public WindowEvent {
   public:
    WindowClosedEvent() = default;
    ~WindowClosedEvent() = default;

    static EventType static_type() { return WindowClose; }
    virtual EventType type() const override { return WindowClose; }
    virtual std::string ToString() const override {
      return std::string("Window closed event");
    }
  };

  class WindowResizedEvent : public WindowEvent {
   public:
    WindowResizedEvent(int width, int height)
        : width_{width}, height_{height} {}

    static EventType static_type() { return WindowResize; }
    virtual EventType type() const override { return WindowResize; }
    virtual std::string ToString() const override;

   private:
    int width_;
    int height_;
  };
}
