#pragma once

#include "events/event.h"

namespace Cerval {
  class MouseMovedEvent : public Event {
   public:
    MouseMovedEvent(double x, double y) : pos_x_{x}, pos_y_{y} {}

    static EventCategory static_category() { return EventCategory::Mouse; }
    static EventType static_type() { return MouseMoved; }
    virtual EventCategory category() const override {
      return EventCategory::Mouse;
    }
    virtual EventType type() const override { return MouseMoved; }
    virtual std::string ToString() const override;

   private:
    double pos_x_;
    double pos_y_;
  };

  class MouseScrolledEvent : public Event {
   public:
    MouseScrolledEvent(double x, double y) : offset_x_{x}, offset_y_{y} {}

    static EventCategory static_category() { return EventCategory::Mouse; }
    static EventType static_type() { return MouseScrolled; }
    virtual EventCategory category() const override {
      return EventCategory::Mouse;
    }
    virtual EventType type() const override { return MouseScrolled; }
    virtual std::string ToString() const override;

   private:
    double offset_x_;
    double offset_y_;
  };

  class MouseButtonEvent : public Event {
   public:
    static EventCategory static_category() {
      return EventCategory::MouseButton;
    }
    virtual EventCategory category() const override {
      return EventCategory::MouseButton;
    }

   protected:
    int button_{};
  };

  class MouseButtonPressedEvent : public MouseButtonEvent {
   public:
    MouseButtonPressedEvent(int button) { button_ = button; }

    static EventType static_type() { return MouseButtonPressed; }
    virtual EventType type() const override { return MouseButtonPressed; }
    virtual std::string ToString() const override {
      return std::string("Mouse button pressed event -> button: ")
           + std::to_string(button_);
    }
  };

  class MouseButtonReleasedEvent : public MouseButtonEvent {
   public:
    MouseButtonReleasedEvent(int button) { button_ = button; }

    static EventType static_type() { return MouseButtonReleased; }
    virtual EventType type() const override { return MouseButtonReleased; }
    virtual std::string ToString() const override {
      return std::string("Mouse button released event -> button: ")
           + std::to_string(button_);
    }
  };
}
