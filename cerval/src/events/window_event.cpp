#include "window_event.h"

namespace Cerval {
  std::string WindowResizedEvent::ToString() const {
    std::string res = std::string("Window resized event -> new size: (")
          + std::to_string(width_)
          + std::string(", ")
          + std::to_string(height_)
          + std::string(")");
    return res;
  }
}
