#include "key_event.h"

namespace Cerval {
  std::string KeyPressedEvent::ToString() const {
    std::string key =
      (65 <= keycode_ && keycode_ <= 90)
      ? std::string{(char)keycode_}
      : std::to_string(keycode_);
    std::string res = std::string("Key pressed event -> keycode: ")
                    + key
                    + std::string(", repeated: ")
                    + std::to_string(repeated_);
    return res;
  }

  std::string KeyReleasedEvent::ToString() const {
    std::string key =
      (65 <= keycode_ && keycode_ <= 90)
      ? std::string{(char)keycode_}
      : std::to_string(keycode_);
    std::string res = std::string("Key released event -> keycode: ")
                    + key;
    return res;
  }
}
