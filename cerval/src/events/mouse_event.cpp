#include "mouse_event.h"

namespace Cerval {
  std::string MouseMovedEvent::ToString() const {
    std::string res = std::string("Mouse moved event -> mouse pos: (")
          + std::to_string(pos_x_)
          + std::string(", ")
          + std::to_string(pos_y_)
          + std::string(")");
    return res;
  }

  std::string MouseScrolledEvent::ToString() const {
    std::string res = std::string("Mouse scrolled event -> scroll offset: (")
          + std::to_string(offset_x_)
          + std::string(", ")
          + std::to_string(offset_y_)
          + std::string(")");
    return res;
  }
}
